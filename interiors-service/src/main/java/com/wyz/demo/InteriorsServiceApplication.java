package com.wyz.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan({"com.wyz.demo.core.infrastructure.db.*.mapper"})
@ComponentScan(basePackages = {"com.wyz.demo.core.**"})
public class InteriorsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(InteriorsServiceApplication.class, args);
    }

}
