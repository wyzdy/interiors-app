package com.wyz.demo.common.result;

/**
 * @Description:
 * @Author: wei yz
 * @Date: 2022/7/17 14:46
 */
public class ResultGenerator {
    public static final String DEFAULT_SUCCESS_MESSAGE = "SUCCESS";

    public static Result genSuccessResult() {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE);
    }

    public static Result genSuccessResult(Object data) {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE)
                .setData(data);
    }

    public static Result genStatusResult(boolean flag) {
        Result result = new Result();
        if (flag) {
            result.setCode(ResultCode.SUCCESS)
                    .setMessage(DEFAULT_SUCCESS_MESSAGE);
        } else {
            result.setCode(ResultCode.INTERNAL_SERVER_ERROR)
                    .setMessage("操作失败");
        }
        return result;
    }

    public static Result genFailResult(String message) {
        return new Result()
                .setCode(ResultCode.FAIL)
                .setMessage(message)
                .setSuccess(false);
    }

    public static Result genFailResult(ResultCode resultCode, String message) {
        return new Result()
                .setCode(resultCode.code)
                .setMessage(message)
                .setSuccess(false);
    }
}
