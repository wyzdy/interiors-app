package com.wyz.demo.common.constant;

/**
 * @Description: Knife4 常量描述
 * @Author: wei yz
 * @Date: 2022/7/9 14:27
 */
public interface Knife4jConstant {

    String TITLE = "天青色等烟雨";

    String DESC = "这是一个简单的练手项目，单机版";

    String URL = "http://www.xxxx.com/";

    String CONTACT = "865230476@qq.com";

    String VERSION = "v1.0.0";

    String GROUP_NAME = "2.X版本";
    /**
     * 控制层路径
     */
    String APIS_PATH = "com.wyz.demo.core.interfaces.controller";
    /**
     * 路径开始标识
     */
    String LAST_INDEX_STR = "com";

}
