package com.wyz.demo.common.result;

/**
 * @Description: 统一返回类型实体
 * @Author: wei yz
 * @Date: 2022/7/17 14:48
 */
public class Result<T> {
    private int code;
    private String message;
    private T t;
    private Boolean success = true;

    public Result setCode(ResultCode resultCode) {
        this.code = resultCode.code;
        return this;
    }

    public int getCode() {
        return code;
    }

    public Result setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return t;
    }

    public Result<T> setData(T t) {
        this.t = t;
        return this;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Result setSuccess(Boolean success) {
        this.success = success;
        return this;
    }
}
