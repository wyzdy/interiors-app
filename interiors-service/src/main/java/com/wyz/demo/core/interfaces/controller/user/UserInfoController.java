package com.wyz.demo.core.interfaces.controller.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.wyz.demo.common.result.Result;
import com.wyz.demo.common.result.ResultGenerator;
import com.wyz.demo.common.vo.user.UserInfoFormVO;
import com.wyz.demo.common.vo.user.UserInfoQueryVO;
import com.wyz.demo.common.vo.user.UserInfoVO;
import com.wyz.demo.core.application.service.UserInfoAsvc;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description:
 * @Author: wei yz
 * @Date: 2022/7/9 14:41
 */
@Api(tags = "用户模块", value = "用户模块")
@RequestMapping("/user")
@RestController
public class UserInfoController {

    private static final Logger log = LoggerFactory.getLogger(UserInfoController.class);

    @Autowired
    private UserInfoAsvc userInfoAsvc;

    /**
     * 详情
     */
    @GetMapping("/info")
    @ApiImplicitParam(name = "id", value = "测试", required = true)
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "详情")
    public Result<String> info(@RequestParam(value = "id") String id) {
        return ResultGenerator.genSuccessResult(userInfoAsvc.info(id));
    }

    /**
     * 分页
     */
    @PostMapping("/page")
    @ApiImplicitParam(name = "query", value = "查询参数")
    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "分页")
    public Result<IPage<UserInfoVO>> page(@RequestBody UserInfoQueryVO queryVO) {
        return ResultGenerator.genSuccessResult(userInfoAsvc.page(queryVO));
    }

    /**
     * 新增
     */
    @PostMapping("/submit")
    @ApiImplicitParam(name = "form", value = "表单", required = true)
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "新增")
    public Result submit(@RequestBody UserInfoFormVO formVO) {
        return ResultGenerator.genStatusResult(userInfoAsvc.submit(formVO));
    }

}
