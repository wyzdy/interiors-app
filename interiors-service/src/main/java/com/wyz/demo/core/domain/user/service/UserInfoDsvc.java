package com.wyz.demo.core.domain.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wyz.demo.common.vo.user.UserInfoFormVO;
import com.wyz.demo.common.vo.user.UserInfoQueryVO;
import com.wyz.demo.common.vo.user.UserInfoVO;

public interface UserInfoDsvc {

    UserInfoVO info(String id);

    boolean submit(UserInfoFormVO formVO);

    IPage<UserInfoVO> page(UserInfoQueryVO queryVO);
}
