package com.wyz.demo.core.application.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wyz.demo.common.vo.user.UserInfoFormVO;
import com.wyz.demo.common.vo.user.UserInfoQueryVO;
import com.wyz.demo.common.vo.user.UserInfoVO;
import com.wyz.demo.core.domain.user.service.UserInfoDsvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoAsvcImpl implements UserInfoAsvc {

    @Autowired
    private UserInfoDsvc userInfoDsvc;

    @Override
    public UserInfoVO info(String id) {
        return userInfoDsvc.info(id);
    }

    @Override
    public boolean submit(UserInfoFormVO formVO) {
        return userInfoDsvc.submit(formVO);
    }

    @Override
    public IPage<UserInfoVO> page(UserInfoQueryVO queryVO) {
        return userInfoDsvc.page(queryVO);
    }
}
