package com.wyz.demo.core.infrastructure.db.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyz.demo.common.vo.user.UserInfoQueryVO;
import com.wyz.demo.common.vo.user.UserInfoVO;
import com.wyz.demo.core.domain.user.entity.UserInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {


    IPage<UserInfoVO> userPage(Page<UserInfoVO> page, @Param("query") UserInfoQueryVO query);

}
