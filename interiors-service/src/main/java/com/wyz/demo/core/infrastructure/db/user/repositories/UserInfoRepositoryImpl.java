package com.wyz.demo.core.infrastructure.db.user.repositories;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wyz.demo.common.vo.user.UserInfoQueryVO;
import com.wyz.demo.common.vo.user.UserInfoVO;
import com.wyz.demo.core.domain.user.entity.UserInfo;
import com.wyz.demo.core.domain.user.repositoris.UserInfoRepository;
import com.wyz.demo.core.infrastructure.db.user.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: wei yz
 * @Date: 2022/10/17 17:48
 */
@Service
public class UserInfoRepositoryImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoRepository {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public IPage<UserInfoVO> page(UserInfoQueryVO queryVO) {
        return userInfoMapper.userPage(new Page<UserInfoVO>(queryVO.getPageNum().longValue(), queryVO.getPageSize().longValue()), queryVO);
    }
}
