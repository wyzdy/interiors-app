package com.wyz.demo.core.domain.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyz.demo.common.vo.user.UserInfoFormVO;
import com.wyz.demo.common.vo.user.UserInfoQueryVO;
import com.wyz.demo.common.vo.user.UserInfoVO;
import com.wyz.demo.core.domain.user.entity.UserInfo;
import com.wyz.demo.core.domain.user.repositoris.UserInfoRepository;
import com.wyz.demo.core.infrastructure.utils.Utils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoDsvcImpl implements UserInfoDsvc {

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    public UserInfoVO info(String id) {
        UserInfoVO infoVO = new UserInfoVO();
        BeanUtils.copyProperties(userInfoRepository.getById(id), infoVO);
        return infoVO;
    }

    @Override
    public boolean submit(UserInfoFormVO formVO) {
        UserInfo info = new UserInfo();
        BeanUtils.copyProperties(formVO, info);
        if (StringUtils.isBlank(info.getId())) {
            info.setId(Utils.upperCaseUUID());
            info.setDeleted(0);
        }
        return userInfoRepository.saveOrUpdate(info);
    }

    @Override
    public IPage<UserInfoVO> page(UserInfoQueryVO queryVO) {
        return userInfoRepository.page(queryVO);
    }
}
