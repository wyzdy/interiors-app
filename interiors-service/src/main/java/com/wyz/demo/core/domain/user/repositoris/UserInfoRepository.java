package com.wyz.demo.core.domain.user.repositoris;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wyz.demo.common.vo.user.UserInfoQueryVO;
import com.wyz.demo.common.vo.user.UserInfoVO;
import com.wyz.demo.core.domain.user.entity.UserInfo;

public interface UserInfoRepository extends IService<UserInfo> {

    IPage<UserInfoVO> page(UserInfoQueryVO queryVO);
}
